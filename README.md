# Marche à suivre pour démarrer avec le projet démo sur un poste ESIG
1. Télécharger chrome driver en version 2.46 (pour être compatible avec la version de Chrome installée à l'ESIG)
    * https://chromedriver.chromium.org/downloads
1. Dézipper dans C:\ESIGUsers
    * Vous devriez avoir un répertoire C:\ESIGUsers\chromedriver_win32_v2.46 avec chromedriver.exe à l’intérieur
1. Faire un fork (copie personnelle) du repository sur bitbucket
    * https://bitbucket.org/esig-profs-info/demo-selenium-2018
1. Lancer IntelliJ Idea
1. Activer plugin Gradle si nécessaire
    * CTRL-ALT-S, Plugins - Gradle (activer)
    * redémarrer
1. New > Project from Version Control > Git
    * URL:  L'URL de votre nouveau repo
1. Définir la Java SDK 8 si nécessaire
    * C:\Program Files\Java\jdk1.8.0_92
1. Dire à Gradle de faire l’import (message en bas à gauche), *attendre* que le tout soit terminé
1. Ouvrir et lancer la classe TestAntraLogin
